﻿using System;


public DarkWilderness()
{
    int growingPlant(int upSpeed, int downSpeed, int desiredHeight)
    {
        int sum = 0;
        int days = 0;
        while (sum < desiredHeight)
        {
            sum = sum + upSpeed;
            days++;
            if (sum >= desiredHeight)
            {
                break;
            }
            sum -= downSpeed;

        }
        return days;
    }
    int knapsackLight(int value1, int weight1, int value2, int weight2, int maxW)
    {
        int max = value1 > value2 ? value1 : value2;
        if (weight1 + weight2 <= maxW)
        {
            return value1 + value2;
        }
        if (weight1 > maxW && weight2 > maxW)
        {
            return 0;
        }
        if (weight1 <= maxW && weight2 <= maxW)
        {
            return max;
        }
        if (weight1 > maxW)
        {
            return value2;
        }
        if (weight2 > maxW)
        {
            return value1;
        }
        return -1;

    }
    string longestDigitsPrefix(string inputString)
    {
        string s = "";
        List<char> l = new List<char>();
        l = inputString.TakeWhile(dig => Char.IsDigit(dig)).ToList<char>();
        for (int i = 0; i < l.Count; i++)
        {
            s = String.Concat(s, l[i]);
        }
        return s;
    }
    int digitDegree(int n)
    {
        int count = 0;
        while (n >= 10)
        {
            count++;
            n = sumOfDigs(n);
        }
        return count;


    }
    int sumOfDigs(int num)
    {
        int sum = 0;
        if (num > 0)
        {
            while (num > 0)
            {
                sum += num % 10;
                num = num / 10;
            }
        }
        return sum;
    }
    bool bishopAndPawn(string bishop, string pawn)
    {
        if (Math.Abs(bishop[0] - pawn[0]) == Math.Abs(bishop[1] - pawn[1])) return true;
        return false;
    }
}

