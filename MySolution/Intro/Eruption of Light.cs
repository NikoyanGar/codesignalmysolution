﻿using System;

public class EruptionofLight
{
    bool isBeautifulString(string inputString)
    {

        char[] ch = inputString.Distinct().ToArray();
        Array.Sort(ch);
        int[] counts = new int[ch.Length];
        for (int i = 0; i < ch.Length; i++)
        {
            for (int j = 0; j < inputString.Length; j++)
            {
                if (ch[i] == inputString[j])
                {
                    counts[i]++;
                }
            }
        }
        if (ch[0] != 'a')
        {
            return false;
        }

        for (int i = 0; i < ch.Length - 1; i++)
        {
            if (ch[i + 1] - ch[i] > 1 || counts[i + 1] > counts[i])
            {
                return false;
            }
        }
        return true;
    }

    string findEmailDomain(string s)
    {
        return s.Substring(s.LastIndexOf('@') + 1);
    }


    string buildPalindrome(string st)
    {
        int i = 0;
        while (!isPalindrome(st))
        {
            st = st.Insert(st.Length - i, $"{st[i]}");
            i++;
        }
        return st;
    }
    bool isPalindrome(string str)
    {
        for (int i = 0; i < str.Length / 2; i++)
        {
            if (!str[i].Equals(str[str.Length - 1 - i]))
            {
                return false;
            }
        }

        return true;
    }
    int electionsWinners(int[] votes, int k)
    {
        int max = votes.Max();
        int countOfMaxs = 0;
        foreach (var item in votes)
        {
            if (item == max)
            {
                countOfMaxs++;
            }
        }
        if (k == 0 && countOfMaxs > 1)
        {
            return 0;
        }
        if (k == 0 && countOfMaxs == 1)
        {
            return 1;
        }
        int count = 0;
        for (int i = 0; i < votes.Length; i++)
        {
            if (votes[i] + k > max)
            {
                count++;
            }
        }
        return count;


    }

    bool isMAC48Address(string inputString)
    {
        string[] st = inputString.Split('-');
        if (st.Length != 6) return false;

        foreach (var str in st)
        {
            if (str.Length != 2) return false;
            foreach (var ch in str)
            {

                if (Char.IsDigit(ch) || ch == 'A' || ch == 'B'
                    || ch == 'C' || ch == 'D'
                    || ch == 'E' || ch == 'F')
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }

        }
        return true;
    }


}
