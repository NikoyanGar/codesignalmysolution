﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolution
{
    class Class3
    {
        string[] allLongestStrings(string[] inputArray)
        {
            List<string> output = new List<string>();
            int maxLenght = inputArray[0].Length;
            for (int i = 1; i < inputArray.Length; i++)
            {
                if (inputArray[i].Length > maxLenght)
                {
                    maxLenght = inputArray[i].Length;
                }
            }
            foreach (string item in inputArray)
            {
                if (item.Length == maxLenght)
                {
                    output.Add(item);
                }
            }
            string[] o = output.ToArray();
            return o;
        }
        int commonCharacterCount(string s1, string s2)
        {

            List<char> lst1 = s1.ToList();
            List<char> lst2 = s2.ToList();
            int count = 0;
            foreach (char c in lst2)
            {
                if (lst1.Contains(c))
                {
                    lst1.Remove(c);
                    count++;
                }
            }
            return count;
        }
        bool isLucky(int n)
        {
            int m = n;
            int sum1 = 0;
            int sum2 = 0;
            int lenght = 0;

            while (m != 0)
            {
                lenght++;
                m /= 10;
            }
            if (lenght % 2 == 1) return false;
            int lenghtsource = lenght;
            while (lenght != lenghtsource / 2)
            {
                sum1 += n % 10;
                n /= 10;
                lenght--;
            }
            while (lenght != 0)
            {
                sum2 += n % 10;
                n /= 10;
                lenght--;
            }
            return sum1 == sum2;
        }
        int[] sortByHeight(int[] a)
        {
            List<int> indexOfTree = new List<int>();
            List<int> values = new List<int>();

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] == -1) indexOfTree.Add(i);
                else values.Add(a[i]);
            }

            values.Sort();
            foreach (int i in indexOfTree)
            {
                values.Insert(i, -1);
            }

            return values.ToArray();

        }
        string reverseInParentheses(string inputString)
        {


            int left = inputString.LastIndexOf('(');
            if (left == -1)
            {
                return inputString;
            }
            else
            {
                int right = inputString.IndexOf(')', left);

                return reverseInParentheses(
                      inputString.Substring(0, left)
                    + new string(inputString.Substring(left + 1, right - left - 1).Reverse().ToArray())
                    + inputString.Substring(right + 1)
                );
            }

        }
    }
}
