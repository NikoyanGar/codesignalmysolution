﻿using System;

public class RainbowofClarity
{
	bool isDigit(char symbol)
    { 
        return Char.IsDigit(symbol);
    }

    string lineEncoding(string s)
    {
        var sb = new StringBuilder(s.Length);
        int counter = 1;
        for (int i = 1; i <= s.Length; i++)
        {
            if (i < s.Length && s[i] == s[i - 1])
            {
                counter++;
            }
            else
            {
                if (counter > 1)
                {
                    sb.Append(counter);
                }
                sb.Append(s[i - 1]);
                counter = 1;
            }
        }
        return sb.ToString();
    }

    int chessKnight(string cell)
    {
        int min1 = Math.Min(Math.Min(cell[0] - 'a', 'h' - cell[0]), 2);
        int min2 = Math.Min(Math.Min(cell[1] - '1', '8' - cell[1]), 2);
        if (min1 + min2 == 0)
        {
            return 2;
        }
        if (min1 + min2 == 1)
        {
            return 3;
        }
        if (min1 + min2 == 2)
        {
            return 4;
        }
        if (min1 + min2 == 3)
        {
            return 6;
        }
        return 8;
    }

    int deleteDigit(int n)
    {
        string s = n.ToString();
        int i;
        for (i = 0; i < s.Length - 1; i++)
            if (s[i] < s[i + 1]) break;
        return Convert.ToInt32(s.Remove(i, 1)); ;
    }
}
