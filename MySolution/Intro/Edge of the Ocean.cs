﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolution
{
    class Class1
    {
        int adjacentElementsProduct(int[] inputArray)
        {
            int max = inputArray[0] * inputArray[1];
            for (int i = 1; i < inputArray.Length - 1; i++)
            {
                if (inputArray[i] * inputArray[i + 1] > max)
                {
                    max = inputArray[i] * inputArray[i + 1];
                }
            }
            return max;

        }
        int shapeArea(int n)
        {
            return n * n + (n - 1) * (n - 1);

        }
        int makeArrayConsecutive2(int[] statues)
        {
            Array.Sort(statues);
            int count = 0;
            for (int i = 0; i < statues.Length - 1; i++)
            {
                while (statues[i + 1] != statues[i] + 1)
                {
                    statues[i] = statues[i] + 1;
                    count++;
                }
            }
            return count;
        }
        bool almostIncreasingSequence(int[] sequence)
        {
            if (sequence.Length == 2) { return true; }
            int countOne = 0;
            int countTwo = 0;
            for (int i = 0; i < sequence.Length - 1; i++)
            {
                if (sequence[i] >= sequence[i + 1])
                {
                    countOne++;
                }
                if (i != 0)
                {
                    if (sequence[i - 1] >= sequence[i + 1])
                    {
                        countTwo++;
                    }
                }
            }

            if (countOne == 1 && countTwo <= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        int matrixElementsSum(int[][] matrix)
        {
            int sum = 0;
            for (int i = 0; i < matrix.Length - 1; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    if (matrix[i][j] == 0)
                    {
                        matrix[i + 1][j] = 0;

                    }
                }
            }
            for (int i = 0; i < matrix.Length; i++)
            {
                foreach (int item in matrix[i])
                {
                    if (item != 0)
                    {
                        sum += item;
                    }
                }
            }
            return sum;

        }


    }
}
