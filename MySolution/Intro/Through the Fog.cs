﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolution
{
    class Through_the_Fog
    {
        int circleOfNumbers(int n, int firstNumber)
        {
            if (firstNumber + n / 2 < n) return firstNumber + n / 2;
            return
                firstNumber - n / 2;

        }
        int depositProfit(int deposit, int rate, int threshold)
        {
            double dep = deposit;
            int counter = 0;
            while (true)
            {
                if (dep >= threshold) return counter;
                dep += dep * rate / 100;
                counter++;
            }
           
        }
        int absoluteValuesSumMinimization(int[] a)
        {
            int[] sums = new int[a.Length];

            for (int item = 0; item < a.Length; item++)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    sums[i] += Math.Abs(a[i] - a[item]);
                }
            }
            int min = sums[0];
            int index = 0;
            for (int i = 1; i < sums.Length; i++)
            {
                if (sums[i] < min)
                {
                    min = sums[i];
                    index = i;
                }
            }
            return a[index];
        }

        #region stringsRearrangement

         bool stringsRearrangement(string[] inputArray) {
    for (int i = 0; i < inputArray.Length; i++)
	{
		var left = inputArray.Where((_, index) => index != i).ToArray();
		if (stringsRearrangementHelper(inputArray[i], left))
			return true;
	}
	
	return false;
}

        bool stringsRearrangementHelper(string lastItem, string[] left) {
    if (left.Length == 0)
		return true;
		
	for (int i = 0; i < left.Length; i++)
	{
		if (Diff(lastItem, left[i]) != 1)
			continue;
		
		var childLeft = left.Where((_, index) => index != i).ToArray();
		if (stringsRearrangementHelper(left[i], childLeft))
			return true;
	}
	
	return false;
}

         int Diff(string s1, string s2)
{
    return s1.Zip(s2, (f,s) => new {f, s}).Count(p => p.f != p.s);
}
        #endregion
    }
}
