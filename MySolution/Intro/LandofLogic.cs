﻿using System;

public class LandofLogic
{
    string longestWord(string text)
    {
        string current = "";
        string longest = "";

        foreach (char c in text)
        {
            if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z')
            {
                current += c.ToString();
            }
            else
            {
                if (current.Length > longest.Length)
                {
                    longest = current;
                }
                current = "";
            }
        }

        return current.Length > longest.Length ? current : longest;
    }

    bool validTime(string time)
    {
        DateTime dateTime = new DateTime();
        if (DateTime.TryParse(time, out dateTime))
        {
            return true;
        }
        return false;


    }

    int sumUpNumbers(string inputString)
    {
        int sum = 0;
        int number = 0;
        foreach (var s in inputString)
        {
            int n;
            if (int.TryParse(s.ToString(), out n))
            {
                number = number * 10 + n;
            }
            else
            {
                sum += number;
                number = 0;
            }
        }
        return sum + number;
    }
    int differentSquares(int[][] matrix)
    {
        List<int> nums = new List<int>();
        for (int i = 0; i < matrix.Length - 1; i++)
        {
            for (int j = 0; j < matrix[i].Length - 1; j++)
            {
                if (i + 1 < matrix.Length && j + 1 < matrix[i].Length)
                {
                    nums.Add(matrix[i][j] * 1000 + matrix[i][j + 1] * 100 + matrix[i + 1][j] * 10 + matrix[i + 1][j + 1]);
                }
            }
        }
        nums = nums.Distinct().ToList();
        return nums.Count;
    }

    int digitsProduct(int product)
    {
        if (product == 0) return 10;
        if (product == 1) return 1;
        string digits = "";
        for (int div = 9; div > 1; div--)
        {
            while (product % div == 0)
            {
                product /= div;
                digits = div.ToString() + digits;
            }
        }

        if (product > 1) return -1;

        return Convert.ToInt32(digits);

    }

    string[] fileNaming(string[] names)
    {
        string[] output = new string[names.Length];
        for (int i = 0; i < names.Length; i++)
        {
            string name = names[i];
            if (Contains(output, name))
            {
                for (int num = 1; num > 0; num++)
                {
                    if (Contains(output, name + $"({num})"))
                        continue;
                    else
                    {
                        output[i] = name + $"({num})";
                        break;
                    }
                }
            }
            else
                output[i] = name;
        }
        return output;
    }
    bool Contains(string[] names, string name)
    {
        for (int i = 0; i < names.Length; i++)
        {
            if (names[i] == name)
                return true;
        }
        return false;
    }

    string messageFromBinaryCode(string code)
    {
        List<Byte> byteList = new List<Byte>();

        for (int i = 0; i < code.Length; i += 8)
        {
            byteList.Add(Convert.ToByte(code.Substring(i, 8), 2));
        }

        return Encoding.ASCII.GetString(byteList.ToArray());
    }
    int[][] spiralNumbers(int n)
    {
        int[][] ans = new int[n][];
        for (int i = 0; i < n; i++)
            ans[i] = new int[n];

        int pointer = 1;
        int rowI = 0;
        for (; n > 0; n -= 2)
        {
            for (int i = 0; i < n; i++)
            {
                ans[rowI][i + rowI] = pointer;
                pointer++;
            }
            for (int j = rowI + 1; j < n + rowI; j++)
            {
                ans[j][n - 1 + rowI] = pointer;
                pointer++;
            }
            for (int k = n - 2 + rowI; k >= rowI; k--)
            {
                ans[n - 1 + rowI][k] = pointer;
                pointer++;
            }
            for (int h = n - 2 + rowI; h > rowI; h--)
            {
                ans[h][rowI] = pointer;
                pointer++;
            }
            rowI++;
        }
        return ans;
    }

    bool sudoku(int[][] grid)
    {
        int[] t = new int[9];
        int s = 0;
        foreach (var i in grid)
            if (i.Length != i.Distinct().Count()) return false;

        for (int j = 0; j < 9; j++)
        {
            for (int i = 0; i < 9; i++)
                t[i] = grid[i][j];

            if (t.Length != t.Distinct().Count()) return false;
        }

        for (int i = 0; i < 9; i += 3)
        {
            for (int j = 0; j < 9; j += 3)
            {
                for (int m = i; m < i + 3; m++)
                    for (int n = j; n < j + 3; n++)
                    {
                        t[s++] = grid[m][n];
                    }

                s = 0;
                if (t.Length != t.Distinct().Count())
                {
                    return false;
                }
            }
        }
        return true;
    }



}
