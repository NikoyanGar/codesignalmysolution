﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolution
{
    class Class4
    {
        int[] alternatingSums(int[] a)
        {
            int sum1 = 0;
            int sum2 = 0;

            for (int i = 0; i < a.Length; i++)
            {
                if (i % 2 == 1)
                {
                    sum2 += a[i];
                }
                else
                {
                    sum1 += a[i];
                }
            }
            int[] sums = new int[] { sum1, sum2 };
            return sums;

        }
        string[] addBorder(string[] picture)
        {
            List<string> l = picture.ToList<string>();
            int max = 0;
            foreach (var item in l)
            {
                if (item.Length > max)
                {
                    max = item.Length;
                }
            }
            for (int i = 0; i < l.Count; i++)
            {
                l[i] = l[i].Insert(0, "*");
                while (l[i].Length != max + 2)
                {
                    l[i] = l[i].Insert(l[i].Length, "*");
                }

            }
            l.Insert(0, new string('*', max + 2));
            l.Insert(l.Count, new string('*', max + 2));
            return l.ToArray();


        }
        bool areSimilar(int[] a, int[] b)

        {
            List<int> index = new List<int>();


            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                {
                    index.Add(i);
                }
            }
            if (index.Count == 1)
            {
                return false;
            }
            if (index.Count > 2)
            {
                return false;
            }
            if (index.Count == 0)
            {
                return true;
            }
            if (index.Count == 2)
            {
                int temp = a[index[0]];
                a[index[0]] = a[index[1]];
                a[index[1]] = temp;
                if (a[index[0]] == b[index[0]] && a[index[1]] == b[index[1]])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else return false;

        }
        int arrayChange(int[] inputArray)
        {
            int count = 0;
            for (int i = 0; i < inputArray.Length - 1; i++)
            {
                while (inputArray[i] >= inputArray[i + 1])
                {
                    inputArray[i + 1]++;
                    count++;
                }
            }
            return count;
        }
        bool palindromeRearranging(string inputString)
        {

            List<char> l = inputString.ToList<char>();

            for (int i = 0; i < l.Count - 1; i++)
            {
                for (int j = i + 1; j < l.Count; j++)
                {
                    if (l[i] == l[j])
                    {
                        l.RemoveAt(j);
                        l.RemoveAt(i);
                        i--;
                        break;


                    }
                }
            }
            if (l.Count == 1 || l.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
