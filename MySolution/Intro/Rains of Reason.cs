﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolution
{
    class Class5
    {
        int[] arrayReplace(int[] inputArray, int elemToReplace, int substitutionElem)
        {
            for (int i = 0; i < inputArray.Length; i++)
            {
                if (inputArray[i] == elemToReplace)
                {
                    inputArray[i] = substitutionElem;
                }
            }
            return inputArray;
        }
        bool evenDigitsOnly(int n)
        {
            int dig = 0;
            while (n > 0)
            {
                dig = n % 10;
                if (dig % 2 == 1)
                {
                    return false;
                }
                n = n / 10;
            }
            return true;
        }
        bool variableName(string name)
        {

            if (Char.IsDigit(name[0]))
            {
                return false;
            }
            for (int i = 0; i < name.Length; i++)
            {
                if (!Char.IsLetter(name[i]) && name[i] != '_' && !Char.IsDigit(name[i]))
                {
                    return false;
                }
            }
            return true;



        }
        string alphabeticShift(string inputString)
        {
            List<char> ret = new List<char>();
            for (int i = 0; i < inputString.Length; i++)
            {
                switch (inputString[i])
                {
                    case 'a':
                        ret.Add('b');
                        break;
                    case 'b':
                        ret.Add('c');
                        break;
                    case 'c':
                        ret.Add('d');
                        break;
                    case 'd':
                        ret.Add('e');
                        break;
                    case 'e':
                        ret.Add('f');
                        break;
                    case 'f':
                        ret.Add('g');
                        break;
                    case 'g':
                        ret.Add('h');
                        break;
                    case 'h':
                        ret.Add('i');
                        break;
                    case 'i':
                        ret.Add('j');
                        break;
                    case 'j':
                        ret.Add('k');
                        break;
                    case 'k':
                        ret.Add('l');
                        break;
                    case 'l':
                        ret.Add('m');
                        break;
                    case 'm':
                        ret.Add('n');
                        break;
                    case 'n':
                        ret.Add('o');
                        break;
                    case 'o':
                        ret.Add('p');
                        break;
                    case 'p':
                        ret.Add('q');
                        break;
                    case 'q':
                        ret.Add('r');
                        break;
                    case 'r':
                        ret.Add('s');
                        break;
                    case 's':
                        ret.Add('t');
                        break;
                    case 't':
                        ret.Add('u');
                        break;
                    case 'u':
                        ret.Add('v');
                        break;
                    case 'v':
                        ret.Add('w');
                        break;
                    case 'w':
                        ret.Add('x');
                        break;
                    case 'x':
                        ret.Add('y');
                        break;
                    case 'y':
                        ret.Add('z');
                        break;
                    case 'z':
                        ret.Add('a');
                        break;
                }
            }
            return String.Concat(ret);
        }
        bool chessBoardCellColor(string cell1, string cell2)
        {
            string k = "A1 A3 A5 A7 B2 B4 B6 B8 C1 C3 C5 C7 D2 D4 D6 D8 E1 E3 E5 E7 F2 F4 F6 F8 G1 G3 G5 G7 H2 H4 H6 H8";
            string s = "A2 A4 A6 A8 B1 B3 B5 B7 C2 C4 C6 C8 D1 D3 D5 D7 E2 E4 E6 E8 F1 F3 F5 F7 G2 G4 G6 G8 H1 H3 H5 H7";
            if (k.Contains(cell1) && k.Contains(cell2) || s.Contains(cell1) && s.Contains(cell2))
            {
                return true;
            }
            return false;

        }
    }
}
