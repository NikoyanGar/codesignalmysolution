﻿using System;

public class DivingDeeper
{
    int[] extractEachKth(int[] inputArray, int k)
    {

        List<int> l = new List<int>(inputArray.Length - inputArray.Length * 1 / k);
        for (int i = 0; i < inputArray.Length; i++)
        {
            if (i % k == k - 1)
            {
                continue;
            }
            l.Add(inputArray[i]);
        }
        return l.ToArray<int>();

    }
    char firstDigit(string inputString)
    {

        for (int i = 0; i < inputString.Length; i++)
        {
            if (Char.IsDigit(inputString[i]))
            {
                return inputString[i];
            }
        }
        return 'z';
    }
    int differentSymbolsNaive(string s)
    {
        List<char> l = s.Distinct().ToList<char>();
        return l.Count;
    }
    int arrayMaxConsecutiveSum(int[] inputArray, int k)
    {
        List<int> sums = new List<int>();
        for (int i = 0; i <= inputArray.Length - k; i++)
        {
            int sum = inputArray[i];
            for (int ka = 1; ka < k; ka++)
            {
                sum += inputArray[i + ka];
            }
            sums.Add(sum);
        }

        return sums.Max();
    }
}
