﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolution
{
    class Island_of_knowledge
    {
        bool areEquallyStrong(int yourLeft, int yourRight, int friendsLeft, int friendsRight)
        {
            int yourmax = 0;
            int yourmin = 0;
            int friendsmax = 0;
            int friendsmin = 0;

            if (yourLeft >= yourRight)
            {
                yourmax = yourLeft; yourmin = yourRight;
            }
            else
            {
                yourmax = yourRight; yourmin = yourLeft;
            }
            if (friendsLeft >= friendsRight)
            {
                friendsmax = friendsLeft; friendsmin = friendsRight;
            }
            else
            {
                friendsmax = friendsRight; friendsmin = friendsLeft;
            }
            if (yourLeft + yourRight == friendsLeft + friendsRight && (yourmax == friendsmax || yourmin == friendsmin))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //bool areEquallyStrong(int yourL, int yourR, int friendsL, int friendsR)
        //{
        //    return (yourL == friendsL && yourR == friendsR)
        //           || (yourL == friendsR && yourR == friendsL);
        //}
        int arrayMaximalAdjacentDifference(int[] inputArray)
        {
            int max = Math.Abs(inputArray[0] - inputArray[1]);
            for (int i = 1; i < inputArray.Length - 1; i++)
            {
                if (Math.Abs(inputArray[i] - inputArray[i + 1]) > max)
                {
                    max = Math.Abs(inputArray[i] - inputArray[i + 1]);
                }
            }
            return max;
        }
        bool isIPv4Address(string inputString)
        {
            string[] nums = inputString.Split(new char[] { '.' });
            List<int> l = new List<int>();
            int count = 0;
            if (nums.Length == 4)
            {
                foreach (var item in nums)
                {
                    try
                    {
                        l.Add(int.Parse(item));
                    }
                    catch (Exception)
                    {

                        return false;
                    }
                }
                foreach (var item in l)
                {
                    if (item >= 0 && item <= 255)
                    {
                        count++;
                    }
                }
            }
            if (count == 4)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        int avoidObstacles(int[] inputArray)
        {
            int distance = 1;
            bool IsDivide = true;
            while (IsDivide)
            {
                IsDivide = false;
                for (int i = 0; i < inputArray.Length; i++)
                {
                    if (inputArray[i] % distance == 0)
                    {
                        distance++;
                        IsDivide = true;
                        break;
                    }
                }
            }
            return distance;
        }
        int[][] boxBlur(int[][] image)
        {
            int[][] result = new int[image.Length - 2][];
            for (int i = 1; i < image.Length - 1; i++)
                for (int j = 1; j < image[0].Length - 1; j++)
                {
                    int sum = 0;
                    for (int ii = i - 1; ii <= i + 1; ii++)
                    {
                        for (int jj = j - 1; jj <= j + 1; jj++)
                        {
                            sum += image[ii][jj];
                        }
                    }
                    result[i - 1][j - 1] = sum / 9;
                }
            return result;
        }
        int[][] minesweeper(bool[][] matrix)
        {
            int[][] nums = new int[matrix.Length][];
            for (int i = 0; i < matrix.Length; i++)
            {
                nums[i] = new int[matrix[i].Length];
            }

            #region amen harevan andamin +1
            for (int i = 0; i < matrix.Length; i++)
            {

                for (int j = 0; j < matrix[i].Length; j++)
                {
                    if (matrix[i][j] == true)
                    {
                        if (i != 0 && i != matrix.Length - 1 && j != 0 && j != matrix[i].Length - 1)
                        {
                            nums[i - 1][j]++;
                            nums[i + 1][j]++;
                            nums[i][j - 1]++;
                            nums[i][j + 1]++;
                            nums[i + 1][j - 1]++;
                            nums[i - 1][j + 1]++;
                            nums[i + 1][j + 1]++;
                            nums[i - 1][j - 1]++;

                        }
                        else if (i != matrix.Length - 1 && j != 0 && j != matrix[i].Length - 1)
                        {
                            nums[i + 1][j]++;
                            nums[i][j - 1]++;
                            nums[i][j + 1]++;
                            nums[i + 1][j - 1]++;
                            nums[i + 1][j + 1]++;

                        }
                        else if (i != 0 && j != 0 && j != matrix[i].Length - 1)
                        {
                            nums[i - 1][j]++;
                            nums[i][j - 1]++;
                            nums[i][j + 1]++;
                            nums[i - 1][j + 1]++;
                            nums[i - 1][j - 1]++;

                        }
                        else if (i != 0 && i != matrix.Length - 1 && j != matrix[i].Length - 1)
                        {
                            nums[i - 1][j]++;
                            nums[i + 1][j]++;
                            nums[i][j + 1]++;
                            nums[i - 1][j + 1]++;
                            nums[i + 1][j + 1]++;
                        }
                        else if (i != 0 && i != matrix.Length - 1 && j != 0)
                        {
                            nums[i - 1][j]++;
                            nums[i + 1][j]++;
                            nums[i][j - 1]++;
                            nums[i + 1][j - 1]++;
                            nums[i - 1][j - 1]++;
                        }
                        else if (i != matrix.Length - 1 && j != matrix[i].Length - 1)
                        {
                            nums[i + 1][j]++;
                            nums[i][j + 1]++;
                            nums[i + 1][j + 1]++;
                        }
                        else if (i != 0 && j != 0)
                        {
                            nums[i - 1][j]++;
                            nums[i][j - 1]++;
                            nums[i - 1][j - 1]++;
                        }
                        else if (i != matrix.Length - 1 && j != 0)
                        {
                            nums[i + 1][j]++;
                            nums[i][j - 1]++;
                            nums[i + 1][j - 1]++;
                        }
                        else if (i != 0 && j != matrix[i].Length - 1)
                        {
                            nums[i - 1][j]++;
                            nums[i][j + 1]++;
                            nums[i - 1][j + 1]++;
                        }




                    }

                }

            }
            #endregion
            return nums;
        }

    }
}
